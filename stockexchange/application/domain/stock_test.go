package domain_test

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/briannqc/backend-101/stockexchange/application/domain"
)

func TestNewSymbol(t *testing.T) {
	tests := []struct {
		name    string
		symbol  string
		wantErr error
	}{
		{
			name:    "GIVEN symbol has less than 4 chars THEN fails",
			symbol:  "APL",
			wantErr: domain.ErrInvalidStockSymbol,
		},
		{
			name:    "GIVEN symbol has more than 4 chars THEN fails",
			symbol:  "APPLE",
			wantErr: domain.ErrInvalidStockSymbol,
		},
		{
			name:    "GIVEN symbol has lowercase chars THEN fails",
			symbol:  "Appl",
			wantErr: domain.ErrInvalidStockSymbol,
		},
		{
			name:    "GIVEN symbol has whitespace chars THEN fails",
			symbol:  "A\tPL",
			wantErr: domain.ErrInvalidStockSymbol,
		},
		{
			name:    "GIVEN symbol has 4 uppercase chars THEN passes",
			symbol:  "AAPL",
			wantErr: nil,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := domain.NewSymbol(tt.symbol)
			if tt.wantErr != nil {
				assert.ErrorIs(t, err, tt.wantErr)
			} else {
				assert.Equal(t, tt.symbol, string(got))
			}
		})
	}
}

func TestNewStock(t *testing.T) {
	tests := []struct {
		name    string
		symbol  string
		company string
		wantErr error
	}{
		{
			name:    "GIVEN company is blank THEN fails",
			symbol:  "AAPL",
			company: "\t   \t",
			wantErr: domain.ErrInvalidStockCompanyName,
		},
		{
			name:    "GIVEN company is not blank THEN passes",
			symbol:  "AAPL",
			company: "Apple Inc.",
			wantErr: nil,
		},
		{
			name:    "GIVEN company starts with blank THEN fails",
			symbol:  "AAPL",
			company: "   Apple Inc.",
			wantErr: domain.ErrInvalidStockCompanyName,
		},
		{
			name:    "GIVEN company ends with blank THEN fails",
			symbol:  "AAPL",
			company: "Apple Inc.   ",
			wantErr: domain.ErrInvalidStockCompanyName,
		},
		{
			name:    "GIVEN invalid symbol THEN fails",
			symbol:  "APL",
			company: "Apple Inc.",
			wantErr: domain.ErrInvalidStockSymbol,
		},
		{
			name:    "GIVEN valid symbol and company name THEN passes",
			symbol:  "AAPL",
			company: "Apple Inc.",
			wantErr: nil,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := domain.NewStock(tt.symbol, tt.company)
			if tt.wantErr != nil {
				assert.ErrorIs(t, err, tt.wantErr)
			} else {
				assert.Equal(t, tt.symbol, string(got.Symbol()))
				assert.Equal(t, tt.company, got.Company())
			}
		})
	}
}
