package service

import (
	"context"
	"gitlab.com/briannqc/backend-101/stockexchange/application/domain"
	"gitlab.com/briannqc/backend-101/stockexchange/application/port/out"
)

type GetOrderService struct {
	authPort  out.CanViewOrderPort
	orderPort out.GetOrderPort
}

func NewGetOrderService(
	authPort out.CanViewOrderPort,
	orderPort out.GetOrderPort,
) *GetOrderService {
	return &GetOrderService{
		authPort:  authPort,
		orderPort: orderPort,
	}
}

func (s *GetOrderService) GetOrder(
	ctx context.Context,
	id string,
) (domain.Order, error) {
	if err := s.authPort.CanViewOrder(ctx, id); err != nil {
		return domain.Order{}, err
	}
	return s.orderPort.GetOrderByID(ctx, id)
}
