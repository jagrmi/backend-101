package service_test

import (
	"context"
	"errors"
	"testing"

	"github.com/golang/mock/gomock"
	"github.com/stretchr/testify/assert"
	"gitlab.com/briannqc/backend-101/stockexchange/application/domain"
	"gitlab.com/briannqc/backend-101/stockexchange/application/domain/stub"
	"gitlab.com/briannqc/backend-101/stockexchange/application/port/out"
	"gitlab.com/briannqc/backend-101/stockexchange/application/service"
)

var (
	anErr = errors.New("any error")
)

func TestPlaceOrderService_PlaceOrder(t *testing.T) {
	type args struct {
		ctx       context.Context
		symbol    string
		quantity  uint
		priceCent uint64
		side      string
	}

	sellOrder := stub.SellOrderNew()
	sellOrderFilled := stub.SellOrderFilled()
	stubID := sellOrder.ID()
	sellArgs := args{
		ctx:       context.Background(),
		symbol:    string(sellOrder.Symbol()),
		quantity:  sellOrder.Quantity(),
		priceCent: uint64(sellOrder.Price()),
		side:      string(domain.SideSell),
	}

	buyOrder := stub.BuyOrderNew()
	buyOrderFilled := stub.BuyOrderFilled()

	type GetOrderBookReturn struct {
		orderBook *domain.OrderBook
		err       error
	}
	tests := []struct {
		name               string
		args               args
		order              domain.Order
		saveOrderErr       error
		getOrderBookReturn GetOrderBookReturn
		wantFilledOrders   []domain.Order
		saveAllOrdersErr   error
		wantErr            error
	}{
		{
			name: "GIVEN invalid args THEN fails",
			args: args{
				ctx:       context.Background(),
				symbol:    "AAPL",
				quantity:  0,
				priceCent: 10000,
				side:      string(domain.SideBuy),
			},
			wantErr: domain.ErrInvalidOrderQuantity,
		},
		{
			name:         "GIVEN save order fails THEN fails",
			args:         sellArgs,
			order:        sellOrder,
			saveOrderErr: anErr,
			wantErr:      anErr,
		},
		{
			name:         "GIVEN get order book fails THEN fails",
			args:         sellArgs,
			order:        sellOrder,
			saveOrderErr: nil,
			getOrderBookReturn: GetOrderBookReturn{
				err: anErr,
			},
			wantErr: anErr,
		},
		{
			name:         "GIVEN no order matches THEN succeeds without update and report",
			args:         sellArgs,
			order:        sellOrder,
			saveOrderErr: nil,
			getOrderBookReturn: GetOrderBookReturn{
				orderBook: stub.MustOrderBook(),
			},
			wantFilledOrders: []domain.Order{},
			wantErr:          nil,
		},
		{
			name:         "GIVEN order matches but update fails THEN fails",
			args:         sellArgs,
			order:        sellOrder,
			saveOrderErr: nil,
			getOrderBookReturn: GetOrderBookReturn{
				orderBook: stub.MustOrderBook(buyOrder),
			},
			wantFilledOrders: []domain.Order{
				sellOrderFilled,
				buyOrderFilled,
			},
			saveAllOrdersErr: anErr,
			wantErr:          anErr,
		},
		{
			name:         "GIVEN order matches and update succeeds THEN report execution and succeeds",
			args:         sellArgs,
			order:        sellOrder,
			saveOrderErr: nil,
			getOrderBookReturn: GetOrderBookReturn{
				orderBook: stub.MustOrderBook(buyOrder),
			},
			wantFilledOrders: []domain.Order{
				sellOrderFilled,
				buyOrderFilled,
			},
			saveAllOrdersErr: nil,
			wantErr:          nil,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			ctrl := gomock.NewController(t)
			generateOrderID := func() string { return stubID }
			orderPort := out.NewMockUpsertOrderPort(ctrl)
			orderBookPort := out.NewMockGetOrderBookPort(ctrl)
			reportExecutionPort := out.NewMockReportExecutionPort(ctrl)

			orderPort.EXPECT().
				InsertOrder(tt.args.ctx, tt.order).
				Return(tt.saveOrderErr).
				MaxTimes(1)
			orderBookPort.EXPECT().
				GetOrderBook(tt.args.ctx).
				Return(tt.getOrderBookReturn.orderBook, tt.getOrderBookReturn.err).
				MaxTimes(1)
			if len(tt.wantFilledOrders) > 0 {
				orderPort.EXPECT().
					UpdateAllOrders(tt.args.ctx, tt.wantFilledOrders).
					Return(tt.saveAllOrdersErr).
					Times(1)
				if tt.saveAllOrdersErr == nil {
					reportExecutionPort.EXPECT().
						ReportExecution(tt.args.ctx, tt.wantFilledOrders).
						Times(1)
				}
			}

			s := service.NewPlaceOrderService(generateOrderID, orderPort, orderBookPort, reportExecutionPort)
			gotID, gotErr := s.PlaceOrder(tt.args.ctx, tt.args.symbol, tt.args.quantity, tt.args.priceCent, tt.args.side)
			if tt.wantErr != nil {
				assert.ErrorIs(t, gotErr, tt.wantErr)
			} else {
				assert.Equal(t, generateOrderID(), gotID)
			}
		})
	}
}
