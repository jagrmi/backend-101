//go:generate mockgen -destination=order_book_mock.go -package=out -source=order_book.go

package out

import (
	"context"

	"gitlab.com/briannqc/backend-101/stockexchange/application/domain"
)

type GetOrderBookPort interface {
	GetOrderBook(context.Context) (*domain.OrderBook, error)
}
