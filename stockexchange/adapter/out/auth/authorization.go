package auth

import (
	"context"
	"log"
)

type StubAuthorizationAdapter struct{}

func (a *StubAuthorizationAdapter) CanViewOrder(_ context.Context, _ string) error {
	log.Println("I'm stub, I bypass all authorization checks")
	return nil
}
